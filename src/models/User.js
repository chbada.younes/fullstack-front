import JwtDecode from 'jwt-decode'

export default class User {
  static from (token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      return null
    }
  }

  constructor ({ id, lastName, firstName, email, 
    login
   }) {
this.id = id // eslint-disable-line camelcase
this.lastName = lastName
this.firstName = firstName
this.email = email
this.login = login
}


  // get isAdmin () {
  //   return this.admin
  // }
}